# js-npm

Test project with:

* **Language:** Javascript
* **Package Manager:** Npm

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :x:                |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  | :white_check_mark: |
| DAST                | :x:                |
| License Management  | :white_check_mark: |

